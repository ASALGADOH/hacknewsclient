import { TestBed } from '@angular/core/testing';

import { GetAllNewsService } from './get-all-news.service';

describe('GetAllNewsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetAllNewsService = TestBed.get(GetAllNewsService);
    expect(service).toBeTruthy();
  });
});
