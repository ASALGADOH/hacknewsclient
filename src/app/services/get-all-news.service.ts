import { Injectable } from '@angular/core';
import { HackNewsAPI } from '../models/news.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetAllNewsService {
  constructor(private http: HttpClient) {}

  ENDPOINT = `http://localhost:9090/node/api`;

  $PARAM = '/news';

  public getAllNews(): Observable<HackNewsAPI[]> {
    const parametros = new HttpParams().set('news', 'all');
    return this.http.get<HackNewsAPI[]>(`${this.ENDPOINT}${this.$PARAM}`, {
      params: parametros
    });
  }
}
