import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor() {}

  public setStorage(key: string, data: any): void {
    localStorage.setItem(key, data);
  }

  public getStorage(key: string) {
    return localStorage.getItem(key);
  }
}
