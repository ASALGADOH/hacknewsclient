import { Component } from '@angular/core';
import { StorageService } from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'HN Feed';
  constructor(private storage: StorageService) {
    // initial state of storage
    const store = this.storage.getStorage('ID');
    if (!store) {
      console.log('FIRST TIME STORAGE');
      const aux: [] = [];
      this.storage.setStorage('ID', JSON.stringify(aux));
    }
  }
}
