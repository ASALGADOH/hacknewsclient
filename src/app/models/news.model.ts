export interface HackNewsAPI {

        created_at: Date;
        title: string;
        url: string;
        author: string;
        points: string;
        story_text: string;
        comment_text: string;
        num_comments: string;
        story_id: number;
        story_title: string;
        story_url: string;
        parent_id: number;
        created_at_i: number;
        _tags: [];
        objectID: string;
        _highlightResult: {
          author: {
            value: string,
            matchLevel: string,
            matchedWords: []
          },
          comment_text: {
            value: string,
            matchLevel: string,
            fullyHighlighted: boolean,
            matchedWords: []
          },
          story_title: {
            value: string,
            matchLevel: string,
            matchedWords: []
          }
        };

}
