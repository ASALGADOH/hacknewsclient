import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { GetAllNewsService } from 'src/app/services/get-all-news.service';
import { HackNewsAPI } from 'src/app/models/news.model';
import * as moment from 'moment';
import { MatTableDataSource } from '@angular/material/table';
import { StorageService } from 'src/app/services/storage.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements AfterViewInit {
  displayedColumns: string[] = ['Titulo', 'Fecha', 'icon'];
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  data: HackNewsAPI[];

  dataSource = new MatTableDataSource<HackNewsAPI>(this.data);

  filteredData: HackNewsAPI[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private newsService: GetAllNewsService,
    private storage: StorageService
  ) {}

  ngAfterViewInit() {
    this.newsService.getAllNews().subscribe(
      async response => {
        response = _.differenceBy(response, JSON.parse(this.storage.getStorage('ID')), 'objectID');
        response.forEach(async (element: HackNewsAPI, index, array) => {
          if (element.title !== null || element.story_title !== null) {
            if (element.created_at) {
              const today = moment().format('DD/MM/YYYY');
              const dateResponseFormat = moment(element.created_at).format(
                'DD/MM/YYYY'
              );
              const yesterday = moment(
                moment()
                  .subtract(1, 'day')
                  .toDate()
              ).format('DD/MM/YYYY');
              if (today === dateResponseFormat) {
                element.created_at = moment(element.created_at).format(
                  'HH:mm a'
                ) as any;
              } else if (dateResponseFormat === yesterday) {
                element.created_at = 'Yesterday' as any;
              } else {
                element.created_at = moment(element.created_at).format(
                  'MMM D'
                ) as any;
              }
            }
            await this.filteredData.push(element);
          }
          if (index === array.length - 1) {
            this.data = this.filteredData;
            this.isLoadingResults = false;
            this.resultsLength = this.data.length;
            this.dataSource = new MatTableDataSource<HackNewsAPI>(this.data);
            this.dataSource.paginator = this.paginator;
          }
        });
      },
      error => {
        this.isLoadingResults = false;
        this.isRateLimitReached = true;
        this.data = [];
      }
    );
  }

  rowSelected(url: string): void {
    console.log(url);
    if (url) {
      window.open(url, '_blank');
    } else {
      alert('URL or STORY_URL DOES NOT CONTAIN DATA');
    }
  }

  clickDelete(row: HackNewsAPI) {
    const banned = JSON.parse(this.storage.getStorage('ID'));
    banned.push(row);
    this.storage.setStorage('ID', JSON.stringify(banned));
    this.reloadDataSource();
  }

  private reloadDataSource(): void {
    const banArray = JSON.parse(this.storage.getStorage('ID'));
    const witOutBanned = _.differenceBy(this.data, banArray, 'objectID');
    this.dataSource = new MatTableDataSource<HackNewsAPI>(witOutBanned);
  }
}
