# HackNewsAngular
````
Detalles:

    Este proyecto angular esta dockerizado para ser ejecutado en entorno linux bajo nginx. Aun asi en la fase 1 puede de igual manera ser empaquetado en una imagen y en la fase 2 se puede modificar para ejecutar en otro entorno diferente a nginx.

    El puerto configurado para escuchar el llamado de API es el 9090, esto puede ser modificado en el archivo de servcio. "app/services/get-all-news.service.ts" linea 12.


    No tiene puerto definido para correr, puede correr en el 4200 con ng serve o bien compilar el dist y montar sobre algun servidor http.

    si presenta problemas node-sass se puede descomentar la siguiente linea de Dockerfile: RUN npm rebuild node-sass. esto hara que vuelva a compilar las hojas de estilo scss.

````