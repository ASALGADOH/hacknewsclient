# stage 1 building
FROM node:latest as node
WORKDIR /HN_ASALGADO_ANGULAR
COPY . .
RUN npm i --unsafe-perm
#RUN npm rebuild node-sass
RUN npm run build --prod

# stage 2 NGINX 
FROM nginx:alpine
COPY --from=node /HN_ASALGADO_ANGULAR/dist/HackNewsAngular /usr/share/nginx/html
